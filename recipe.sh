#!/usr/bin/env bash

set -euo pipefail
set -x

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

script_dir=$(dirname $(readlink -f $0))
mkdir -p $out_dir

checkout()
{
    if [ -d audacity ]; then
        return
    fi

    git clone https://github.com/audacity/audacity
    pushd audacity 
    git checkout $version
    git log -n1
    popd
}

configure_conan()
{
    python -m pip install conan
    export CONAN_HOME=$(cygpath -m $(pwd))/conan

    if [ -f conan/configured ]; then
        return
    fi
    
    rm -rf conan
    mkdir -p conan

    local bash=$(cygpath -m $(which bash.exe))
    conan profile detect
cat > global.conf << EOF
tools.microsoft.bash:path=$bash
tools.microsoft.bash:subsystem=msys2
EOF
    conan config install global.conf
    rm global.conf

    touch conan/configured
}

build()
{
    pushd audacity 
    mkdir -p build

    pushd build
    if [ ! -f configured ]; then
        cmake -A arm64 -S ../ -B .
        touch configured
    fi
    cmake --build .
    find | grep exe$ | xargs file # list binaries
    popd
}

configure_conan
checkout
build