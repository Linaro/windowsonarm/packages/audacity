import shutil
import subprocess
import sys
from datetime import datetime
from os.path import dirname, join

from packagetools.builder import *
from packagetools.command import cygwin_path, run

# Enabled from: (will be included in 3.5)
# https://github.com/audacity/audacity/commit/cb2677ae64b48d00a29e119aa3117b0fee6f603b
VERSION = "cb2677a"
RECIPE_DIR = dirname(__file__)


class Recipe(PackageShortRecipe):
    def describe(self) -> PackageInfo:
        return PackageInfo(
            description="Audacity",
            id="audacity",
            pretty_name="Audacity",
            version=VERSION,
        )

    def all_steps(self, out_dir: str):
        bash = shutil.which("bash")
        recipe = cygwin_path(join(RECIPE_DIR, "recipe.sh"))
        run(bash, recipe, VERSION, cygwin_path(out_dir))


PackageBuilder(Recipe()).make()
